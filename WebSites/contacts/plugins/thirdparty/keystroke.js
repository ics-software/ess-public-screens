let buffer = [];
let lastKeyTime = Date.now();

document.addEventListener('keydown', event => {
    const key = event.key.toLowerCase();
    const currentTime = Date.now();

    if (currentTime - lastKeyTime > 1000) {
            buffer = [];
    }

    buffer.push(key);
    lastKeyTime = currentTime;
    if (buffer.join('') === 'edit') {
        window.open('https://crss.tn.esss.lu.se/', '_blank');
    }
});
